<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::get('/colores', function() {
    $colores = ['rojo', 'azul', 'amarillo'];
    $color = $colores[array_rand($colores)];

    $logLevels = ['info', 'warning', 'error', 'debug'];
    $randomLogLevel = $logLevels[array_rand($logLevels)];
    Log::{$randomLogLevel}('Color seleccionado: ' . $color);

    return response()->json(['color' => $color]);
});
